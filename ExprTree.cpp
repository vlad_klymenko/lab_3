#include "ExprTree.h"
#include "stdafx.h"
#include "Visitor.h"

using namespace std;

UNode::~UNode()
{
	delete pArg;
}

BNode::~BNode()
{
	delete pArgR;
	delete pArgL;
}

TNode& TNode::operator = (int iVal)
{
	data.type = INT;
	data.varData.i = iVal;
	return *this;
}

TNode& TNode::operator = (double dVal)
{
	data.type = DBL;
	data.varData.d = dVal;
	return *this;
}

TNode& TNode::operator = (char* str)
{
	data.type = NCONST;
	strncpy(data.varData.namedConst, str, MAX_NAMED_CONST_LEN);
	return *this;
}

void UNode::setFunction(char* a){
	char *c = new char[10];
	for (int i = 0; i < 10; i++)
	{
		c[i] = 0;
	}
	for (int i = 0; i < strlen(a); i++)
	{
		c[i] = a[i];
	}
	this->fncName = c;

};

void TNode::Accept(CalcVisitor& theVisitor, AResult*pResult) {
	CalcResult& left_value = *((CalcResult*)pResult);
	nodeVariant A;
	(this)->getData(A);
	if (A.type == INT)
	{
		left_value.value = A.varData.i;
		//cout << "BNode=" << A.varData.i << endl;
	}
	if (A.type == DBL)
	{
		left_value.value = A.varData.d;
		//cout << "BNode=" << A.varData.d << endl;
	}
};

void BNode::Accept(CalcVisitor& theVisitor, AResult*pResult) {
	((CalcVisitor*)(&theVisitor))->Visit(this, pResult);
};
void UNode::Accept(CalcVisitor& theVisitor, AResult*pResult) {
	((CalcVisitor*)(&theVisitor))->Visit(this, pResult);
}