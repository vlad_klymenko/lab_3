#pragma once
#include "ExprTypes.h"
#include "TreeBuilder.h"
#include "ExprTree.h"
Operand*  CTreeBuilder::BuildTerminal(Memento* pCurrentState)
{
	Operand *currentNode = 0;
	if (pCurrentState->token == INUMBER)
	{
		currentNode = new TNode();
		*dynamic_cast<TNode*>(currentNode) = atoi(pCurrentState->tokenStr);
		return currentNode;
	}
	if (pCurrentState->token == DNUMBER)
	{
		currentNode = new TNode();
		*dynamic_cast<TNode*>(currentNode) = atof(pCurrentState->tokenStr);
		return currentNode;
	}

}
Operand*  CTreeBuilder::BuildUnaryOperation(Memento* pCurrentState, Operand* pLeft)
{
	UNode* currentNode;

	if (pCurrentState->token == MINUS)
	{
		currentNode = new UNode(UMIN);
	}
	else
	{
		currentNode = new UNode(FNC);
		currentNode->setFunction(pCurrentState->tokenStr);
	}
	currentNode->setLeft((ANode*)pLeft);

	return (Operand*)currentNode;
}
Operand* CTreeBuilder::BuildBinaryOperation(token_value token, Operand* pLeft, Operand* pRight)
{
	Operand* currentNode = pLeft;
		switch (token)
		{
		case PLUS:
			currentNode = new BNode(ADD);
			break;
		case MINUS:
			currentNode = new BNode(MIN);
			break;
		case MUL:
			currentNode = new BNode(MULT);
			break;
		case DIV:
			currentNode = new BNode(DIVD);
			break;
		default:
			return currentNode;
		}
		currentNode->setLeft(pLeft);
		currentNode->setRight(pRight);
	return currentNode;
}