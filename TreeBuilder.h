#pragma once
#include "ExprTypes.h"
#include "stdafx.h"
//#include "ExprTree.h"
class CTreeBuilder : public AExprBuilder
{

public:
	virtual Operand* BuildTerminal(Memento* pCurrentState);
	virtual Operand* BuildUnaryOperation(Memento* pCurrentState, Operand* pLeft);
	virtual Operand* BuildBinaryOperation(token_value token, Operand* pLeft, Operand* pRight);

};
