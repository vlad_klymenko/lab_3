#ifndef __ExprTree_h__
#define __ExprTree_h__

#include "ExprTypes.h"
#include "stdafx.h"
class AVisitor;
struct AResult{};

class ANode :public Operand{
public:
	virtual Operand* getRight() const = 0;
	virtual Operand* getLeft() const = 0;
	virtual void setRight(Operand*) = 0;
	virtual void setLeft(Operand*) = 0;
	virtual void Accept(CalcVisitor& theVisitor, AResult*) = 0;

};


const int MAX_NAMED_CONST_LEN = 9;
struct nodeVariant {
	DataType type;
	union{
		int i;
		double d;
		char namedConst[MAX_NAMED_CONST_LEN + 1];
	} varData;
};

class TNode : public ANode
{
private:
	nodeVariant data;
public:
	TNode() {  };
	~TNode() {};

	virtual Operand* getRight() const { return NULL; };
	virtual Operand* getLeft() const { return NULL; };
	virtual void setRight(Operand*) {};
	virtual void setLeft(Operand*) {};

	void getData(nodeVariant& d) { d.type = data.type; d.varData = data.varData; }

	TNode& operator = (int);
	TNode& operator = (double);
	TNode& operator = (char*);
	virtual void Accept(CalcVisitor& theVisitor, AResult*);

};



class UNode : public ANode
{
private:
	Operand* pArg;
	UOperation operation;
	char* fncName = NULL;
public:
	UNode(UOperation o) : pArg(NULL), operation(o) {  };
	~UNode();

	virtual Operand* getRight() const { return pArg; }
	virtual Operand* getLeft() const { return NULL; }
	virtual void setRight(Operand* pNode) { pArg = pNode; }
	virtual void setLeft(Operand* pNode) { pArg = pNode; }

	UOperation getOperation() const { return operation; }
	inline char* getFunction() const { return fncName; }
	void setFunction(char*);

	virtual void Accept(CalcVisitor& theVisitor, AResult*) ;
};


class BNode : public ANode
{
private:
	Operand* pArgR;
	Operand* pArgL;
	BOperation operation;

public:
	BNode(BOperation o) : pArgR(NULL), pArgL(NULL), operation(o) { };
	~BNode();

	virtual Operand* getRight() const { return pArgR; }
	virtual Operand* getLeft() const { return pArgL; }
	virtual void setRight(Operand* pNode) { pArgR = pNode; }
	virtual void setLeft(Operand* pNode) { pArgL = pNode; }
	BOperation getBOperation() const { return operation; }
	virtual void Accept(CalcVisitor& theVisitor, AResult*);

};

class AVisitor
{
public:
	virtual void Visit(TNode*, AResult*) =0;
	virtual void Visit(UNode*, AResult*) =0;
	virtual void Visit(BNode*, AResult*) =0;
};

#endif