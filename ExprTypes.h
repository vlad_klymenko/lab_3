// Expression types definition

#ifndef __ExprTypes_h__
#define __ExprTypes_h__
#include <iostream>
const double pi = 3.1415926535897932385;



enum DataType {
	INT, 
	DBL, 
	NCONST
};

enum UOperation
{
	UMIN = '-',
	FNC
};

enum BOperation{
	ADD = '+', 
	MIN = '-', 
	MULT = '*', 
	DIVD = '/', 
	DIVI = '\\', 
};

enum OperandType{
	isTerminal, isUnary, isBinary
};

class CalcVisitor;
struct AResult;
class Operand{
	public:
		virtual Operand* getRight() const = 0;
		virtual Operand* getLeft() const = 0;
		virtual void setRight(Operand*) = 0;
		virtual void setLeft(Operand*) = 0;
		virtual void Accept(CalcVisitor& theVisitor, AResult*) = 0;
	};
	



enum token_value {
    NAME = NCONST,		DNUMBER = DBL,	INUMBER = INT,	BEGIN = 100,	END = 101,
    PLUS='+',	MINUS='-',  MUL='*',	DIV='/',	IDIV='\\', 
	LP = '(', RP = ')'
};

struct Memento
{
	token_value token;
	char* tokenStr=0;
	virtual ~Memento() {
		if (tokenStr == 0);
		else delete tokenStr;
	}
};

class AExprBuilder
{
public:
	virtual Operand* BuildTerminal(Memento* pCurrentState) = 0;
	virtual Operand* BuildUnaryOperation(Memento* pCurrentState, Operand* pLeft) = 0;
	virtual Operand* BuildBinaryOperation(token_value token, Operand* pLeft, Operand* pRight) = 0;
};

#endif

