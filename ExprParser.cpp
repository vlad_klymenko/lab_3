#pragma once
#include "ExprParser.h"
#include "ExprTree.h"
#include "stdafx.h"
token_value CExprParser::GetToken(char*& fromSymbol, char* pBufer)
{
	if (!(*fromSymbol)) return END;
	if (isdigit(*fromSymbol))
	{
		do{
			*pBufer++ = *fromSymbol++;
		} while (isdigit(*fromSymbol));
		if (*fromSymbol != '.')
		{
			*pBufer = 0;
			return INUMBER;
		}
		else
		{
			*pBufer++ = '.';
			if (isdigit(*(++fromSymbol)))
			{
				do{
					*pBufer++ = *fromSymbol++;
				} while (isdigit(*fromSymbol));
			}
			*pBufer = 0;
			return DNUMBER;
		}
	}
	else
	{
		switch (*fromSymbol)
		{
		case '*':
		case '/':
		case '\\':
		case '+':
		case '-':
		case '(':
		case ')':
		{
			token_value token = token_value(fromSymbol[0]);
			fromSymbol++;
			return token;
		}
		default: //FUNCTION NAMES
		{
			if (isalpha(*fromSymbol))
				do{
					*pBufer++ = *fromSymbol++;
				} while (isalpha(*fromSymbol));
				*pBufer = 0;
				return NAME;
		}
		}
	}
}

Operand* CExprParser::expr()
{
	Operand *left = term();
	Operand *currentNode = left;
	Memento currentState;
    currentState.token = currentToken;
   if (currentState.token == PLUS || currentState.token == MINUS)
   {
	   currentToken = GetToken();
	   currentNode= theBuilder->BuildBinaryOperation(currentState.token, left, expr());
   }
   return currentNode;
	//for (;;)
	//{
	//	switch (currentToken)
	//	{
	//	case PLUS:
	//		currentNode = new BNode(ADD);
	//		break;
	//	case MINUS:
	//		currentNode = new BNode(MIN);
	//		break;
	//	default:
	//		return currentNode;
	//	}
	//	currentNode->setLeft(left);
	//	currentToken = GetToken();
	//	currentNode->setRight(expr());
	//}

}

Operand* CExprParser::term()
{
	Operand *left = prim();
	Operand *currentNode = left;
	Memento currentState;
	currentState.token = currentToken;
	if (currentState.token == MULT || currentState.token == DIVD || currentState.token == DIVI)
	{
		currentToken = GetToken();
		currentNode = theBuilder->BuildBinaryOperation(currentState.token, left, term());
	}
	return currentNode;
	//for (;;)
	//{
	//	switch (currentToken)
	//	{
	//	case MUL:
	//		currentNode = new BNode(MULT);
	//		break;
	//	case DIV:
	//		currentNode = new BNode(DIVD);
	//		break;
	//	case IDIV:
	//		currentNode = new BNode(DIVI);
	//		break;
	//	default:

	//		return currentNode;
	//	}
	//	currentNode->setLeft(left);
	//	currentToken = GetToken();
	//	currentNode->setRight(term());

	//}
}

Operand* CExprParser::prim()
{
	//Operand *currentNode = NULL;

	//switch (currentToken)
	//{
	//case INUMBER:
	//	currentNode = new TNode();
	//	std::cout << atoi(bufer) << std::endl;
	//	*dynamic_cast<TNode*>(currentNode) = atoi(bufer);
	//		*((TNode*)(currentNode)) = atoi(bufer);
	//	currentToken = GetToken();
	//	return currentNode;
	//case DNUMBER:
	//	currentNode = new TNode();
	//	*dynamic_cast<TNode*>(currentNode) = atof(bufer);
	//	currentToken = GetToken();
	//	return currentNode;
	//case NAME:
	//	currentNode = new UNode(FNC);
	//	currentToken = GetToken();
	//	currentNode->setLeft(prim());
	//	return currentNode;
	//case MINUS: //Unary minus
	//	currentNode = new UNode(UMIN);
	//	currentToken = GetToken();
	//	currentNode->setLeft(prim());
	//	return currentNode;
	//case LP:
	//	currentToken = GetToken();
	//	currentNode = expr();
	//	if (currentToken != RP) return NULL;
	//	currentToken = GetToken();
	//	return currentNode;
	//case END:
	//	return NULL;
	//default:
	//	return NULL;
	//}

	Memento currentState;
	currentState.tokenStr = strcpy(new char[strlen(bufer) + 1], bufer);
	currentState.token = currentToken;
	currentToken = GetToken();

	switch (currentState.token)
	{
	case INUMBER:
	case DNUMBER:
		return theBuilder->BuildTerminal(&currentState);
	case MINUS: //Unary minus
		return theBuilder->BuildUnaryOperation(&currentState, prim());
	case NAME:
		if (currentToken != LP)
			return theBuilder->BuildTerminal(&currentState);
		else
			return theBuilder->BuildUnaryOperation(&currentState, prim());
	case LP:
	{
		Operand* currentOperand = expr();
		if (currentToken != RP) return NULL;
		currentToken = GetToken();
		return currentOperand;
	}
	case END:
		return NULL;
	default:
		return NULL;
	}

}

Operand* CExprParser::CreateTree(char* exprStr)
{
	pCurrentPosition = exprStr;
	currentToken = BEGIN;
	currentToken = GetToken();
	return treeHead = expr();
}