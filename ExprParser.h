#ifndef __ExprParser_h__
#define __ExprParser_h__

#include "ExprTypes.h"

const int MAX_TERM_STR_LEN = 1024;
class CExprParser
{
	char* pCurrentPosition;
	char bufer[4 +1];
	token_value currentToken;
	
	Operand* treeHead;

	AExprBuilder* theBuilder;

protected:
	virtual token_value GetToken(char*& fromSymbol, char* pBufer);
	inline token_value GetToken()
			{return GetToken(pCurrentPosition, bufer);}

	Operand* expr();
	Operand* term();
	Operand* prim();

public:
	CExprParser(AExprBuilder* pBuilder) : theBuilder(pBuilder) {};
	Operand* CreateTree(char* expr);
};

#endif