#pragma once
#include "Visitor.h"
CalcVisitor* CalcVisitor::_instance = NULL;
STRING2FNC CalcVisitor::function;
void CalcVisitor::Visit(BNode* pNode, AResult* pResult)
{
	(pNode->getLeft())->Accept(*this, pResult);
	CalcResult& left_value = *((CalcResult*)pResult);
	//std::cout << left_value.value << std::endl;
	CalcResult right_value;
	(pNode->getRight())->Accept(*this, (AResult*)(&right_value));
	//std::cout << right_value.value << std::endl;
	switch (pNode->getBOperation())
	{
	case ADD:
		left_value.value = left_value.value + right_value.value;
		break;
	case MIN:
		left_value.value = left_value.value - right_value.value;
		break;
	case MULT:
		left_value.value = left_value.value * right_value.value;
		break;
	case DIVD:
	case DIVI:
		left_value.value = left_value.value / right_value.value;
		break;
	}
}

void CalcVisitor::Visit(UNode* pNode, AResult* pResult)
{
//	cout << pNode->getFunction() << endl;
	function[pNode->getFunction()] ;
	MATH_FNC fnc = function[pNode->getFunction()];
	(pNode->getRight())->Accept(*this, pResult);
	if (fnc) ((CalcResult*)pResult)->value = fnc(((CalcResult*)pResult)->value);
}

void CalcVisitor::Visit(TNode* a, AResult*pResult)
{

	CalcResult& left_value = *((CalcResult*)pResult);
	nodeVariant A;
	(a)->getData(A);
	if (A.type == INT)
	{
		left_value.value = A.varData.i;
		//cout << "BNode=" << A.varData.i << endl;
	}
	if (A.type == DBL)
	{
		left_value.value = A.varData.d;
		//cout << "BNode=" << A.varData.d << endl;
	}

}
bool _pairS2F_Less(_pairS2F* left, _pairS2F* right){

	return (strcmp(left->first, right->first) < 0);
}
double CalcVisitor::CalculateTree(ANode* head)
{
	AResult* pResult =new CalcResult();
	if (typeid(*(head)).name() == (string)"class BNode")
	{
		Visit((BNode*)head, pResult);
	}
	if (typeid(*(head)).name() == (string)"class UNode")
	{
		Visit((UNode*)head, pResult);
	}
	
	if (typeid(*(head)).name() == (string)"class TNode")
	{
		Visit((TNode*)head, pResult);
	}
	
	//CalculateTree((ANode*)head->getRight());
	//CalculateTree((ANode*)head->getLeft());
	
	return ((CalcResult*)pResult)->value;
}
_pairS2F fnc_array[] = {
	_pairS2F("sin", sin),
	_pairS2F("cos", cos),
	_pairS2F("tg", tan),
	_pairS2F("exp", exp),
	_pairS2F("lg", log10),
	_pairS2F("ln", log),
	_pairS2F("arctg", atan),
	_pairS2F("arcsin", asin),
	_pairS2F("arccos", acos)
};
const int FNC_COUNT = 9;
CalcVisitor* CalcVisitor::Instance()
{
	//typedef pair<string, MATH_FNC> _pairS2F;
	if (_instance == NULL)
	{
		CalcVisitor::_instance = new CalcVisitor;
		for (int i = 0; i<FNC_COUNT; i++)
		{
			function.push_back(&fnc_array[i]);
		}
		function.DoSort();
	}
	else{}
	return _instance;
	
}
MATH_FNC STRING2FNC::operator [](char* key)
{
	iterator Result;
	Result = lower_bound(begin(), end(), key);
	//URGENT!!! - ������������ ��������
	// if(!strcmp((*Result).first, key)) 
	// Result ���������� ������� 
	// �� ������� ����� �������� ������� pointer_to_find
	// ��� ��������� ��������������� ������� !!! ������� ����� 	// �������� �� ��������� �������
	// ���� pointer_to_find �� ������

	if (Result != end() && !strcmp((*Result)->first, key))
		return (*Result)->second;
	else
		return NULL;
}




